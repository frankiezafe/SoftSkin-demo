                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:2250162
Inch Salt SWP210DWSS LIGHT by shivinteger is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

**FAQ**: http://www.thingiverse.com/shivinteger/about 

[>>>] Generating Attributions log: 
 
[0] Thing Title : BEETHOVEN NIGHT LIGHT
[0] Thing URL : http://www.thingiverse.com/thing:7984
[0] Thing Author : 2ROBOTGUY
[0] Thing Licence : Creative Commons - Attribution

[1] Thing Title : Bolt Hobbing Tool for Lathe
[1] Thing URL : http://www.thingiverse.com/thing:21424
[1] Thing Author : Mindfab
[1] Thing Licence : Creative Commons - Attribution

[2] Thing Title : Tektronix TDS2014 button cap
[2] Thing URL : http://www.thingiverse.com/thing:39909
[2] Thing Author : AnthonyT
[2] Thing Licence : Creative Commons - Attribution

[3] Thing Title : 7 inch vinyl adapter-Thomas-daft punk
[3] Thing URL : http://www.thingiverse.com/thing:93971
[3] Thing Author : kogiel-mogiel
[3] Thing Licence : Creative Commons - Attribution

[4] Thing Title : Martini Glass Clip
[4] Thing URL : http://www.thingiverse.com/thing:210397
[4] Thing Author : tc_fea
[4] Thing Licence : Creative Commons - Attribution

[5] Thing Title : Measuring Cup for Aquarium Salt - 1 scoop per 5 gallons
[5] Thing URL : http://www.thingiverse.com/thing:387793
[5] Thing Author : joshuas
[5] Thing Licence : Creative Commons - Attribution

[6] Thing Title : Ben Lego brick
[6] Thing URL : http://www.thingiverse.com/thing:433497
[6] Thing Author : georgeh1ll
[6] Thing Licence : Creative Commons - Attribution

[7] Thing Title : Opening Help / Opener for disabled people
[7] Thing URL : http://www.thingiverse.com/thing:1076600
[7] Thing Author : evilmaker
[7] Thing Licence : Creative Commons - Attribution

[8] Thing Title : ZX Spectrum Universal Parallel Interface case
[8] Thing URL : http://www.thingiverse.com/thing:1252385
[8] Thing Author : ikonko
[8] Thing Licence : Creative Commons - Attribution

[9] Thing Title : Draper SWP210DWSS submersible pump impellers
[9] Thing URL : http://www.thingiverse.com/thing:1278317
[9] Thing Author : kenkelso
[9] Thing Licence : Creative Commons - Attribution

[10] Thing Title : Puz-O-Fraction + MakerEd Project
[10] Thing URL : http://www.thingiverse.com/thing:1337921
[10] Thing Author : mhache21
[10] Thing Licence : Creative Commons - Attribution

[11] Thing Title : Shape Stencil 
[11] Thing URL : http://www.thingiverse.com/thing:1367593
[11] Thing Author : Relish01
[11] Thing Licence : Creative Commons - Attribution

[12] Thing Title : Upholstery Verb dollar
[12] Thing URL : http://www.thingiverse.com/thing:1372082
[12] Thing Author : shivinteger
[12] Thing Licence : Creative Commons - Attribution

[13] Thing Title : Ghostbusters Belt Gizmo
[13] Thing URL : http://www.thingiverse.com/thing:1407415
[13] Thing Author : azurial
[13] Thing Licence : Creative Commons - Attribution

[14] Thing Title : Toilet Paper Handler
[14] Thing URL : http://www.thingiverse.com/thing:1412970
[14] Thing Author : kalareor
[14] Thing Licence : Creative Commons - Attribution

[15] Thing Title : VHF Antenna replacements
[15] Thing URL : http://www.thingiverse.com/thing:1459096
[15] Thing Author : ddThingz
[15] Thing Licence : Creative Commons - Attribution

[16] Thing Title : LEGO Set Screw Pen Holder
[16] Thing URL : http://www.thingiverse.com/thing:1486961
[16] Thing Author : thetinkeringstudio
[16] Thing Licence : Creative Commons - Attribution

[17] Thing Title : Siyah for a Turkish-Style Bow
[17] Thing URL : http://www.thingiverse.com/thing:1792682
[17] Thing Author : TPiatek360
[17] Thing Licence : Creative Commons - Attribution

[+] Attributions logged 


[+] Getting object #0
[<] Importing : downloads/1380887/dodeca-sum_two.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: -0.88%, F: -1.32%
[-] Object is malformed
[+] Deleting bad source : downloads/1380887/dodeca-sum_two.stl

[+] Getting object #0
[<] Importing : downloads/93971/7_inch_vinyl_adapter-Thomas-daft_punk__repaired.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: -0.00%, F: -0.00%
[+] Preparing : 7 inch vinyl adapter-Thomas-daft punk  repaired

[+] Getting object #1
[<] Importing : downloads/53669/cage_support_straight.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Cage Support Straight
[+] Rotating object = Cage Support Straight
[!] Volume difference is 130.71%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.000
[+] Staging objects : BOTH_BOTTOM
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.000 & Cage Support Straight
[!] Number of faces #1 : 1126 + number of faces #2 : 28 = 1154
[!] Number of faces of union: 1793
[+] Cleaning non manifold
[!] Total removed V: -99.03%, E: -99.44%, F: -99.50%
[-] Object is malformed
[-] Union did not work well. Discarding object.

[+] Getting object #2
[<] Importing : downloads/1278317/Draper_Pump_Impeller_V2.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Draper Pump Impeller V2
[+] Rotating object = Draper Pump Impeller V2
[!] Volume difference is 2211.57%
[+] Scaled Draper Pump Impeller V2 by 76.75%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.000
[+] Staging objects : DEFAULT
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.000 & Draper Pump Impeller V2
[!] Number of faces #1 : 1126 + number of faces #2 : 39670 = 40796
[!] Number of faces of union: 38486
[+] Cleaning non manifold
[!] Total removed V: -0.03%, E: -0.01%, F: -0.00%

[+] Getting object #3
[<] Importing : downloads/1412970/Toilet_Paper_Handler.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Toilet Paper Handler
[+] Rotating object = Toilet Paper Handler
[!] Volume difference is 441.24%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.001
[+] Staging objects : BOTH_LEFT
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.001 & Toilet Paper Handler
[!] Number of faces #1 : 38485 + number of faces #2 : 30300 = 68785
[!] Number of faces of union: 64422
[+] Cleaning non manifold
[!] Total removed V: -0.00%, E: -0.00%, F: 0.00%

[+] Getting object #4
[+] No STL in that folder

[+] Getting object #4
[<] Importing : downloads/7984/BEETHOVEN_HOLE_4_22_2011.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : BEETHOVEN HOLE 4 22 2011
[+] Rotating object = BEETHOVEN HOLE 4 22 2011.001
[!] Volume difference is 18.04%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.000
[+] Staging objects : BOTH_LEFT
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.000 & BEETHOVEN HOLE 4 22 2011.001
[!] Number of faces #1 : 64422 + number of faces #2 : 12304 = 76726
[!] Number of faces of union: 73275
[+] Cleaning non manifold
[!] Total removed V: -0.00%, E: -0.00%, F: 0.00%

[+] Getting object #5
[<] Importing : downloads/1252385/jira_upi_vrch_napis.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Jira Upi Vrch Napis
[+] Rotating object = Jira Upi Vrch Napis
[!] Volume difference is 93.30%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.001
[+] Staging objects : ON_BOTTOM
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.001 & Jira Upi Vrch Napis
[!] Number of faces #1 : 73275 + number of faces #2 : 1766 = 75041
[!] Number of faces of union: 74837
[+] Cleaning non manifold
[!] Total removed V: -0.00%, E: -0.00%, F: -0.00%

[+] Getting object #6
[<] Importing : downloads/21424/holder.STL
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : holder
[+] Rotating object = holder
[!] Volume difference is 2.50%
[+] Scaled holder by 158.64%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.000
[+] Staging objects : ON_BOTTOM
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.000 & holder
[!] Number of faces #1 : 74836 + number of faces #2 : 384 = 75220
[!] Number of faces of union: 75194
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #7
[<] Importing : downloads/1407415/nixie_shelf.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Nixie Shelf
[+] Rotating object = Nixie Shelf
[!] Volume difference is 9.73%
[+] Scaled Nixie Shelf by 100.92%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.001
[+] Staging objects : ON_BACK
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.001 & Nixie Shelf
[!] Number of faces #1 : 75194 + number of faces #2 : 21110 = 96304
[!] Number of faces of union: 96354
[+] Cleaning non manifold
[!] Total removed V: -0.18%, E: -0.13%, F: -0.10%

[+] Getting object #8
[<] Importing : downloads/210397/wine_glass_clip_131209b.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Wine Glass Clip 131209B
[+] Rotating object = Wine Glass Clip 131209B.005
[!] Volume difference is 0.40%
[+] Scaled Wine Glass Clip 131209B.005 by 293.05%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.000
[+] Staging objects : ON_TOP
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.000 & Wine Glass Clip 131209B.005
[!] Number of faces #1 : 96254 + number of faces #2 : 224 = 96478
[!] Number of faces of union: 95020
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #9
[<] Importing : downloads/387793/lairds_customizable_measuring_cup_v3_20140707-13494-10vwr7n-0.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: -0.09%, F: -0.13%
[+] Preparing : Lairds Customizable Measuring Cup V3 20140707-13494-10Vwr7N-0
[+] Rotating object = Lairds Customizable Measuring Cup V3 20140707-13494-10Vwr7N-0
[!] Volume difference is 10.60%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.001
[+] Staging objects : ON_BACK
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.001 & Lairds Customizable Measuring Cup V3 20140707-13494-10Vwr7N-0
[!] Number of faces #1 : 95020 + number of faces #2 : 771 = 95791
[!] Number of faces of union: 95576
[+] Cleaning non manifold
[!] Total removed V: -0.11%, E: -0.07%, F: -0.06%

[+] Getting object #10
[<] Importing : downloads/1337921/Puz-O-Pieces.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Puz-O-Pieces
[+] Rotating object = Puz-O-Pieces.001
[!] Volume difference is 0.20%
[+] Scaled Puz-O-Pieces.001 by 366.96%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.000
[+] Staging objects : ON_TOP
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.000 & Puz-O-Pieces.001
[!] Number of faces #1 : 95522 + number of faces #2 : 276 = 95798
[!] Number of faces of union: 95801
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #11
[<] Importing : downloads/1367593/stencil1.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Stencil1
[+] Rotating object = Stencil1
[!] Volume difference is 1.82%
[+] Scaled Stencil1 by 176.41%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.001
[+] Staging objects : ON_LEFT
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.001 & Stencil1
[!] Number of faces #1 : 95801 + number of faces #2 : 604 = 96405
[!] Number of faces of union: 96405
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #12
[<] Importing : downloads/1792682/Turkish_Siyah_Model_v0.2.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Turkish Siyah Model v0.2
[+] Rotating object = Turkish Siyah Model v0.2
[!] Volume difference is 0.00%
[+] Scaled Turkish Siyah Model v0.2 by 4507.91%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.000
[+] Staging objects : ON_BOTTOM
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.000 & Turkish Siyah Model v0.2
[!] Number of faces #1 : 96405 + number of faces #2 : 416 = 96821
[!] Number of faces of union: 96844
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #13
[<] Importing : downloads/1250500/double_tap.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Double Tap
[+] Rotating object = Double Tap
[!] Volume difference is 0.04%
[+] Scaled Double Tap by 643.21%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.001
[+] Staging objects : BOTH_BACK
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.001 & Double Tap
[!] Number of faces #1 : 96844 + number of faces #2 : 8410 = 105254
[!] Number of faces of union: 106445
[+] Cleaning non manifold
[!] Total removed V: -58.93%, E: -57.37%, F: -56.37%
[-] Object is malformed
[-] Union did not work well. Discarding object.

[+] Getting object #14
[<] Importing : downloads/433497/giveaway_20140819-10399-1yweg0r-0.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: -0.10%, F: -0.15%
[+] Preparing : Giveaway 20140819-10399-1Yweg0R-0
[+] Rotating object = Giveaway 20140819-10399-1Yweg0R-0
[!] Volume difference is 0.01%
[+] Scaled Giveaway 20140819-10399-1Yweg0R-0 by 943.30%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.001
[+] Staging objects : ON_RIGHT
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.001 & Giveaway 20140819-10399-1Yweg0R-0
[!] Number of faces #1 : 96844 + number of faces #2 : 2656 = 99500
[!] Number of faces of union: 99522
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #15
[<] Importing : downloads/1444299/Wall_Tunnel_45_deep_V3_repaired.stl
[+] Cleaning non manifold
[!] Total removed V: -75.97%, E: -81.06%, F: -83.02%
[-] Object is malformed
[+] Deleting bad source : downloads/1444299/Wall_Tunnel_45_deep_V3_repaired.stl

[+] Getting object #15
[<] Importing : downloads/1486961/LEGOTinkeringPenHolder-3.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : LEGOTinkeringPenHolder-3
[+] Rotating object = LEGOTinkeringPenHolder-3
[!] Volume difference is 0.01%
[+] Scaled LEGOTinkeringPenHolder-3 by 1053.02%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.000
[+] Staging objects : BOTH_BOTTOM
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.000 & LEGOTinkeringPenHolder-3
[!] Number of faces #1 : 99522 + number of faces #2 : 17946 = 117468
[!] Number of faces of union: 117536
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #16
[<] Importing : downloads/1372082/object.stl
[+] Cleaning non manifold
[!] Total removed V: -0.02%, E: -0.36%, F: -0.53%
[+] Preparing : Object
[+] Reducing the number of polygons by 77.70%
[+] Rotating object = Object.001
[!] Volume difference is 6.97%
[+] Scaled Object.001 by 112.77%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.001
[+] Staging objects : BOTH_FRONT
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.001 & Object.001
[!] Number of faces #1 : 117536 + number of faces #2 : 100489 = 218025
[!] Number of faces of union: 212901
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: -0.00%, F: -0.00%

[+] Getting object #17
[<] Importing : downloads/1076600/griffhilfe_r1.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Griffhilfe R1
[+] Rotating object = Griffhilfe R1
[!] Volume difference is 0.20%
[+] Scaled Griffhilfe R1 by 366.84%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.000
[+] Staging objects : BOTH_BOTTOM
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.000 & Griffhilfe R1
[!] Number of faces #1 : 212900 + number of faces #2 : 9298 = 222198
[!] Number of faces of union: 222176
[+] Cleaning non manifold
[!] Total removed V: -0.00%, E: -0.00%, F: 0.00%

[+] Getting object #18
[<] Importing : downloads/127029/Building_repaired.stl
[+] Cleaning non manifold
[!] Total removed V: -7.12%, E: -7.70%, F: -7.70%
[-] Object is malformed
[+] Deleting bad source : downloads/127029/Building_repaired.stl

[+] Getting object #18
[<] Importing : downloads/1459096/4-vhf-slip-ons.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : 4-Vhf-Slip-Ons
[+] Rotating object = 4-Vhf-Slip-Ons.001
[!] Volume difference is 0.00%
[+] Scaled 4-Vhf-Slip-Ons.001 by 2036.54%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.001
[+] Staging objects : ON_RIGHT
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.001 & 4-Vhf-Slip-Ons.001
[!] Number of faces #1 : 222176 + number of faces #2 : 304 = 222480
[!] Number of faces of union: 222513
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #19
[<] Importing : downloads/39909/buttoncap.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Buttoncap
[+] Rotating object = Buttoncap
[!] Volume difference is 0.00%
[+] Scaled Buttoncap by 2223.42%
[+] Created duplicate : 7 inch vinyl adapter-Thomas-daft punk  repaired.000
[+] Staging objects : ON_TOP
[+] Union between : 7 inch vinyl adapter-Thomas-daft punk  repaired.000 & Buttoncap
[!] Number of faces #1 : 222513 + number of faces #2 : 28 = 222541
[!] Number of faces of union: 222542
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[>] Exporting STL to : /uploads/20170415-0221-53