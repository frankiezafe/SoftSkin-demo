tool

extends Spatial

export(bool) var auto_locate = false
export(bool) var enable_parenting = false setget _enable_parenting
export(String) var obj = "" setget _obj
export(String) var group = "" setget _group

var prev_lg = null

func _enable_parenting(b):
	enable_parenting = b
	_group(group)

func _obj(s):
	obj = s

func _group(s):
	group = s
	var skin = get_node( obj )
	if skin != null:
		if prev_lg != null:
			prev_lg.parent( skin )
		if not enable_parenting:
			return
		var lg = skin.get_ligaments( group )
		if lg != null:
			var p = lg.get_barycenter()
			p = skin.transform.xform( p )
			if auto_locate:
				global_transform.origin = p
			lg.parent( self )
			prev_lg = lg

func _ready():
	_group(group)
	pass # Replace with function body.
