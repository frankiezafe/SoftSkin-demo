tool
extends Spatial

export(String) var skin_path = "" setget set_skin_path

var reload = false
var tree = []

func set_skin_path(s):
	skin_path = s
	reload = true

func get_children_recursive( parent ):
	for c in parent.get_children():
		tree.append({'name':c.name,'node':c})
		get_children_recursive( c )

func link():
	tree = []
	var skin = get_node( skin_path )
	if skin != null and skin is Softskin:
		get_children_recursive( self )
		# releasing all previous links between ligaments and nodes
		var lgs = skin.get_ligament_groups()
		for n in lgs:
			var lg = skin.get_ligaments(n)
			lg.parent = skin
		
		# building relation between childrens and skin ligaments
		for c in tree:
			var lg = skin.get_ligaments( c.name )
			if lg != null:
				lg.parent = c.node
				print( lg, " linked to ", c.node )
			else:
				print( "no ligaments group called ", c.name )

func _ready():
	pass # Replace with function body.

func _process(delta):
	if reload:
		reload = false
		link()
		
