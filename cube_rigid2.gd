extends RigidBody

var skin = null
var debug = null

var freq_min = 0.001
var freq_max = 0.52632
var freq = freq_min

var color_min = Color(1,1,1,1)
var color_max = Color(1,0,0,1)

var emit_min = Color(0,0,0,1)
var emit_max = Color(0.7,0.55,0,1)

func _ready():
	skin = get_node( "cube_skin" )
	debug = get_node( "cube_debug" )
	pass

func _process(delta):
	
	if (skin != null):
		
		var l = skin.feedback().length()
		var new_f = freq_min + min( 3, l ) / 3 * (freq_max-freq_min)
		if new_f > freq:
			freq += (new_f - freq) * delta * 5
		else:
			freq += (new_f - freq) * delta * 0.5
		skin.set_tensor_frequency( freq )
		var pc = ( freq - freq_min ) / ( freq_max - freq_min )
#		skin.main_material().albedo_color = color_min.linear_interpolate( color_max, pc * pc )
		skin.main_material().emission = emit_min.linear_interpolate( emit_max, pc )
		skin.main_material().emission_energy = pc * pc
		skin.main_material().roughness = pc * pc
#		skin.main_material().metallic = pc
		
		apply_impulse( get_translation(), skin.feedback() * 0.03 )
		
		if ( Input.is_action_just_pressed( "key_space" ) ):
			var main_displayed = skin.main_display()
			skin.set_main_display( !main_displayed )
			skin.set_fiber_display( main_displayed )
			skin.set_tensor_display( main_displayed )
			skin.set_ligament_display( main_displayed )
		
	pass
