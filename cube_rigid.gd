extends RigidBody

var skin = null
var debug = null

func _ready():
	skin = get_node( "cube_skin" )
	debug = get_node( "cube_debug" )
	pass

func _process(delta):
	if (skin != null):
		apply_impulse( get_translation(), skin.feedback() * 0.03 )
		if ( Input.is_action_just_pressed( "key_space" ) ):
			var main_displayed = skin.main_display()
			skin.set_main_display( !main_displayed )
			skin.set_fiber_display( main_displayed )
			skin.set_tensor_display( main_displayed )
			skin.set_ligament_display( main_displayed )
	pass
