extends Spatial

var ff_debug
var cube_rigid
var cube_skin

func _ready():
	
	ff_debug = get_node( "force_feedback" )
	cube_rigid = get_node( "skin_server/cube_rigid" )
	cube_skin = get_node( "skin_server/cube_rigid/cube_skin" )

	pass

func _process(delta):
	
	ff_debug.translation = cube_rigid.get_translation() + cube_skin.feedback()
	
	pass
